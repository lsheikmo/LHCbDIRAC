.. image:: _static/DIRAC_logo.png
  :width: 300
  :target: https://gitlab.cern.ch/lhcb-dirac

.. The following raw setting for the Guide enlarged title

.. raw:: html

   <style>  p.first { font-size:150%; }
   </style>

=========================================
LHCbDIRAC Documentation
=========================================

The `LHCbDIRAC <https://gitlab.cern.ch/lhcb-dirac>`_ project is the LHCb Grid solution.
LHCbDIRAC is DIRAC extension.

DIRAC forms a layer between a particular community and various compute resources to allow optimized,
transparent and reliable usage. LHCbDIRAC specializes DIRAC for LHCb.

  - DIRAC documentation: `<http://dirac.readthedocs.io/en/latest/index.html>`_
  - DIRAC hosted repository: `<https://github.com/DIRACGrid>`_

LHCbDIRAC is the LHCb extension to DIRAC:

  - LHCbDIRAC documentation: `<http://lhcb-dirac.readthedocs.io/en/latest/index.html>`_
  - LHCbDIRAC hosted repository: `<https://gitlab.cern.ch/lhcb-dirac>`_


.. toctree::
   :hidden:

   DevsGuide/tree.rst
   AdministratorGuide/tree.rst
   Certification/tree.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
